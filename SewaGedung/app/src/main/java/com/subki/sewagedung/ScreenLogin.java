package com.subki.sewagedung;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.subki.sewagedung.model.ModelUser;
import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.Helper;

import java.util.List;

public class ScreenLogin extends AppCompatActivity implements ModelUser.OnLoginListener {

    private InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_login);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

    }

    private void login() {
        String username, password;
        EditText ed = (EditText) findViewById(R.id.username);
        username = ed.getText().toString();
        ed = (EditText) findViewById(R.id.password);
        password = ed.getText().toString();

        Log.d("Login", username+" "+password);

        ModelUser.userLogin(username, password, this);
    }

    private void register() {
        startActivity(new Intent(ScreenLogin.this, ScreenRegister.class));
    }

    @Override
    public void OnLogin(ModelUser.Item itemList, Exception e, Throwable t) {
        if(e==null && t==null){
            Helper.writeString(Constan.USER_ID, itemList.userId);
            Helper.writeString(Constan.USER_NAME, itemList.fullName);
            Helper.writeString(Constan.USER_EMAIL, itemList.email);

            startActivity(new Intent(ScreenLogin.this, ScreenMain.class));
            finish();
        }else{
            if(e!=null) Helper.showError(ScreenLogin.this, "Exception Error : "+e.toString());
            if(t!=null) Helper.showError(ScreenLogin.this, "Throwable Error : "+t.toString());
        }
    }
}
