package com.subki.sewagedung;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.subki.sewagedung.adapter.RuangAdapter;
import com.subki.sewagedung.adapter.RuangAdapterList;
import com.subki.sewagedung.model.ModelRuang;
import com.subki.sewagedung.utils.DividerItemDecoration;
import com.subki.sewagedung.utils.Helper;
import com.subki.sewagedung.utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subki on 02/03/2017.
 */

public class FragmentTipe extends Fragment implements ModelRuang.OnRuanganListener, RuangAdapterList.OnItemClickListener, ModelRuang.OnRuanganCategoryListener, AdapterView.OnItemSelectedListener, ModelRuang.OnRuanganBySizeListener {

    private static final String TAG = "FragmentTipe";

    private View root;
    private List<ModelRuang.Item> itemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RuangAdapterList mAdapter;


    private Spinner spinner;
    private String size;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_tipe,container,false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        mAdapter = new RuangAdapterList(getContext(), itemList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        recyclerView.setAdapter(mAdapter);


        spinner = (Spinner) root.findViewById(R.id.spinner);

        ModelRuang.getCategoryRuangan(this);
        return root;
    }

    @Override
    public void OnRuangan(List<ModelRuang.Item> itemList, Exception e, Throwable t) {
        Log.d(TAG, "OnRuangan");
        if(e==null && t==null){
            this.itemList.clear();
            this.itemList.addAll(itemList);
            mAdapter.notifyDataSetChanged();
        }else{
            if(e!=null) Helper.showError(getContext(), "Exception Error : "+e.toString());
            if(t!=null) Helper.showError(getContext(), "Throwable Error : "+t.toString());
        }
    }

    @Override
    public void OnItemClick(ModelRuang.Item item, int position) {
        ScreenRuangDetail.startActivy(getContext(), item);
    }

    @Override
    public void OnRuanganCategory(String[] list, Exception e, Throwable t) {
        Log.d(TAG, "OnRuanganCategory");
        if(e==null && t==null){

            ArrayAdapter<String> adapters = new ArrayAdapter<String>(getActivity(), R.layout.list_spin, list);
            adapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapters);
            spinner.setOnItemSelectedListener(this);

        }else{
            if(e!=null) Helper.showError(getContext(), "Exception Error : "+e.toString());
            if(t!=null) Helper.showError(getContext(), "Throwable Error : "+t.toString());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0){
            ModelRuang.getRuangan(this);
        }else{
            ModelRuang.getRuanganBySize(spinner.getSelectedItem().toString(), this);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void OnRuanganBySize(List<ModelRuang.Item> itemList, Exception e, Throwable t) {
        Log.d(TAG, "OnRuangan");
        if(e==null && t==null){
            this.itemList.clear();
            this.itemList.addAll(itemList);
            mAdapter.notifyDataSetChanged();
        }else{
            if(e!=null) Helper.showError(getContext(), "Exception Error : "+e.toString());
            if(t!=null) Helper.showError(getContext(), "Throwable Error : "+t.toString());
        }
    }
}
