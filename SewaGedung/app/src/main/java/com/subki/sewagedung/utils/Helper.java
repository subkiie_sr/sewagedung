package com.subki.sewagedung.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;

import com.subki.sewagedung.AppMain;
import com.subki.sewagedung.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by MR on 16/8/2016.
 */
public class Helper {
    static final String TAG = "Helper";


    public static Retrofit retrofit(){
        Log.d(TAG, "Connecting to :"+Constan.BASE_API_URL);
        Retrofit retrofit;
        retrofit = new Retrofit.Builder()
                .baseUrl(Constan.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
    public static float dp2Px(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static void showError(Context context, String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Error")
                .setMessage(text)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    public static String readString(String name, String def){
        Context context = AppMain.getContext();
        SharedPreferences sharedPref = context.getSharedPreferences(Constan.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPref.getString(name, def);
    }

    public static int readInt(String name, int def){
        Context context = AppMain.getContext();
        SharedPreferences sharedPref = context.getSharedPreferences(Constan.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPref.getInt(name, def);
    }

    public static void writeString(String name, String value){
        Log.i(TAG, "writeString " + name +"=" + value);
        Context context = AppMain.getContext();
        SharedPreferences sharedPref = context.getSharedPreferences(Constan.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static void writeInt(String name, int value){
        Log.i(TAG, "writeInt " + name +"=" + value);
        Context context = AppMain.getContext();
        SharedPreferences sharedPref = context.getSharedPreferences(Constan.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(name, value);
        editor.apply();
    }

    //convertion date
    public static long[] selisihWaktu(Date waktuSatu, Date waktuDua) {
        long selisihMS = Math.abs(waktuSatu.getTime() - waktuDua.getTime());
        long selisihDetik = selisihMS / 1000 % 60;
        long selisihMenit = selisihMS / (60 * 1000) % 60;
        long selisihJam = selisihMS / (60 * 60 * 1000) % 24;
        long selisihHari = selisihMS / (24 * 60 * 60 * 1000);

        Log.d(TAG, "Perhitungan Selisih "+String.valueOf(selisihHari)+" "+String.valueOf(selisihJam)+" "+String.valueOf(selisihMenit)+" "+String.valueOf(selisihDetik));

        long selisih[] = {selisihHari, selisihJam, selisihMenit,selisihDetik};
        return selisih;
    }

    public static ProgressDialog progressDialog(Context context) {
        ProgressDialog pd = new ProgressDialog(context, R.style.ThemeAlert);
        pd.setCancelable(true);
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        pd.setIndeterminate(true);
        pd.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.custom_progress));

        return pd;
    }

    public static String FormatCurrency(String country, double price){
        String harga = null;

        switch (country){
            case "indonesia":
                DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
                DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
                kursIndonesia.setMaximumFractionDigits(0);

                formatRp.setCurrencySymbol("Rp. ");
                formatRp.setMonetaryDecimalSeparator(',');
                formatRp.setGroupingSeparator('.');

                kursIndonesia.setDecimalFormatSymbols(formatRp);
                harga = kursIndonesia.format(price);
                break;
        }

        return harga;
    }

}
