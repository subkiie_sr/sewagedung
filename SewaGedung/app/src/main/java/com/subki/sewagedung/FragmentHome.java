package com.subki.sewagedung;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.subki.sewagedung.adapter.RuangAdapter;
import com.subki.sewagedung.model.ModelRuang;
import com.subki.sewagedung.utils.GridSpacingItemDecoration;
import com.subki.sewagedung.utils.Helper;
import com.subki.sewagedung.utils.SimpleAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subki on 02/03/2017.
 */

public class FragmentHome extends Fragment implements RuangAdapter.OnItemClickListener, ModelRuang.OnRuanganListener {

    private static final String TAG = "FragmentHome";

    private View root;
    private GridLayoutManager mLayoutManager;
    private List<ModelRuang.Item> mList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RuangAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home,container,false);


        recyclerView = (RecyclerView) root.findViewById(R.id.listRuang);
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        mAdapter = new RuangAdapter(getContext(), mList, this);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, (int) Helper.dp2Px(1, getContext()), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        ModelRuang.getRuangan(this);

        return root;
    }

    @Override
    public void OnItemClick(ModelRuang.Item item, int position) {
//        Toast.makeText(getContext(), "Item Clicked "+position, Toast.LENGTH_LONG).show();
        ScreenRuangDetail.startActivy(getContext(), item);
    }

    @Override
    public void OnRuangan(List<ModelRuang.Item> itemList, Exception e, Throwable t) {
        if(e == null && t ==null){
            mList.clear();
            mList.addAll(itemList);
            mAdapter.notifyDataSetChanged();
        }
    }
}
