package com.subki.sewagedung.model;

import android.util.Log;

import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by subki on 09/04/2017.
 */

public class ModelRuang {

    public static final String TAG = "ModelRuang";

    public interface OnRuanganListener{
        void OnRuangan(List<Item> itemList, Exception e, Throwable t);
    }
    public interface OnRuanganCategoryListener{
        void OnRuanganCategory(String[] list, Exception e, Throwable t);
    }
    public interface OnRuanganBySizeListener{
        void OnRuanganBySize(List<Item> itemList, Exception e, Throwable t);
    }
    public static class Item{
        public String ruangId;
        public String ruangName;
        public String description;
        public String size;
        public int price;
        public String urlImage;
    }

    public static void getRuangan(final OnRuanganListener listener){
        String function = "ruangan";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.listAllRuangan(function);
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                List<Item> items = null;
                try {
                    JSONArray arr = new JSONArray(str);
                    items = new ArrayList<>();
                    for(int i=0; i<arr.length(); i++){
                        Item item = new Item();
                        JSONObject object = arr.getJSONObject(i);
                        Log.d(TAG, object.toString());
                        if(object.has("ruangId")) item.ruangId = object.getString("ruangId");
                        if(object.has("ruangName")) item.ruangName = object.getString("ruangName");
                        if(object.has("description")) item.description = object.getString("description");
                        if(object.has("size")) item.size = object.getString("size");
                        if(object.has("price")) item.price = object.getInt("price");
                        if(object.has("urlImage")) item.urlImage = Constan.BASE_IMAGE+object.getString("urlImage");
                        items.add(item);
                    }
                    listener.OnRuangan(items, null, null);
                } catch (JSONException e) {
                    listener.OnRuangan(items, e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnRuangan(null, null, t);
            }
        });
    }

    public static void getRuanganBySize(String size, final OnRuanganBySizeListener listener){
        String function = "ruanganSize";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.ruanganBySize(function, size);
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                List<Item> items = null;
                try {
                    JSONArray arr = new JSONArray(str);
                    items = new ArrayList<>();
                    for(int i=0; i<arr.length(); i++){
                        Item item = new Item();
                        JSONObject object = arr.getJSONObject(i);
                        Log.d(TAG, object.toString());
                        if(object.has("ruangId")) item.ruangId = object.getString("ruangId");
                        if(object.has("ruangName")) item.ruangName = object.getString("ruangName");
                        if(object.has("description")) item.description = object.getString("description");
                        if(object.has("size")) item.size = object.getString("size");
                        if(object.has("price")) item.price = object.getInt("price");
                        if(object.has("urlImage")) item.urlImage = Constan.BASE_IMAGE+object.getString("urlImage");
                        items.add(item);
                    }
                    listener.OnRuanganBySize(items, null, null);
                } catch (JSONException e) {
                    listener.OnRuanganBySize(items, e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnRuanganBySize(null, null, t);
            }
        });
    }

    public static void getCategoryRuangan(final OnRuanganCategoryListener listener){
        String function = "ruanganCat";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.ruanganCategory(function);
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                String[] list = null;
                try {
                    JSONArray arr = new JSONArray(str);
                    list = new String[arr.length()+1];
                    list[0] = "Show All";
                    for(int i=0; i<arr.length(); i++){
                        JSONObject object = arr.getJSONObject(i);
                        Log.d(TAG, object.toString());
                        if(object.has("size")) list[i+1] = object.getString("size");
                    }
                    listener.OnRuanganCategory(list, null, null);
                } catch (JSONException e) {
                    listener.OnRuanganCategory(list, e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnRuanganCategory(null, null, t);
            }
        });
    }
}
