package com.subki.sewagedung;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.subki.sewagedung.adapter.RuangAdapter;
import com.subki.sewagedung.model.ModelPemesanan;
import com.subki.sewagedung.model.ModelRuang;
import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.Helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pl.polak.clicknumberpicker.ClickNumberPickerListener;
import pl.polak.clicknumberpicker.ClickNumberPickerView;
import pl.polak.clicknumberpicker.PickerClickType;

public class ScreenPesanRuang extends AppCompatActivity implements ModelPemesanan.OnSavePemesananListener, ModelRuang.OnRuanganListener, RuangAdapter.OnItemClickListener {

    public static ModelRuang.Item itemTemp = new ModelRuang.Item();

    public static void startActivy(Context context, ModelRuang.Item item){

        itemTemp = item;

        Intent intent = new Intent(context, ScreenPesanRuang.class);
        intent.putExtra("ruangId", item.ruangId);
        intent.putExtra("ruangName", item.ruangName);
        intent.putExtra("description", item.description);
        intent.putExtra("size", item.size);
        intent.putExtra("price", item.price);
        intent.putExtra("urlImage", item.urlImage);
        context.startActivity(intent);
    }

    private Toolbar toolbar;
    private Calendar calendar;
    private DatePickerDialog.OnDateSetListener dt;
    private InputMethodManager imm;
    private TextView tanggal, ruangan;
    private String tgl, ruangId;
    private ClickNumberPickerView pickerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_pesan_ruang);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pemesanan Ruangan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ruangan = (TextView) findViewById(R.id.ruangan);
        tanggal = (TextView) findViewById(R.id.tanggal);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);


        calendar = Calendar.getInstance();
        dt = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
                tanggal.setText(sdf.format(calendar.getTime()));
                tgl = sdf2.format(calendar.getTime());
            }
        };

        String ruang = itemTemp.ruangName;
        ruangId = itemTemp.ruangId;

        ruangan.setText(ruang);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        tanggal.setText(sdf.format(new Date()));

        pickerView = (ClickNumberPickerView) findViewById(R.id.durasi);

        ruangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpRuangan();
            }
        });

        tanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog pickerDialog = new DatePickerDialog(ScreenPesanRuang.this, dt,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                pickerDialog.getDatePicker().setLayoutMode(1);
                pickerDialog.show();
            }
        });

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ScreenPesanRuang.this, "Saved", Toast.LENGTH_SHORT).show();
                submit();
            }
        });
    }

    List<ModelRuang.Item> itemList = new ArrayList<>();
    RecyclerView recyclerView;
    RuangAdapter mAdapter;
    android.app.Dialog dialogRuangan;
    private void popUpRuangan() {
        ModelRuang.getRuangan(this);

    }

    private void submit() {
        String userId = Helper.readString(Constan.USER_ID,"");
        String durasi = ""+pickerView.getValue();
        ModelPemesanan.savePemesanan(ruangId, userId, tgl, durasi, this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnSaved(String result, Exception e, Throwable t) {
        if(e == null && t == null){
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Booking Ruangan anda berhasil dilakukan, silahkan lakukan pembayaran dalam kurun waktu 1x24 jam. \nJika melebihi batas waktu yang ditentukan, makan pemesanan anda akan kami batalkan.")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            builder.show();
        }else {
            if(e!=null) Log.d("TEst1", e.toString());
            if(t!=null) Log.d("TEst2", t.toString());
        }
    }

    @Override
    public void OnRuangan(List<ModelRuang.Item> itemList, Exception e, Throwable t) {
        if(e==null && t==null){
            dialogRuangan = new android.app.Dialog(ScreenPesanRuang.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialogRuangan.setContentView(R.layout.fragment_ruangan);
            dialogRuangan.show();

            mAdapter = new RuangAdapter(ScreenPesanRuang.this, itemList, this);

            recyclerView = (RecyclerView) dialogRuangan.findViewById(R.id.list);

            GridLayoutManager mLayoutManager = new GridLayoutManager(this, 3);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(mAdapter);
        }else{
            if(e!=null) Log.d("TEst1", e.toString());
            if(t!=null) Log.d("TEst2", t.toString());
        }
    }

    @Override
    public void OnItemClick(ModelRuang.Item item, int position) {
        ruangan.setText(item.ruangName);
        ruangId = item.ruangId;
        dialogRuangan.dismiss();
    }
}
