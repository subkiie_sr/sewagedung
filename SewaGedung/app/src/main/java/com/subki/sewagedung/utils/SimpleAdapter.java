package com.subki.sewagedung.utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Created by Erick on 3/16/2017.
 */

public class SimpleAdapter extends BaseAdapter {

    public interface ISimpleAdapterCallback {
        public int onGetCount(SimpleAdapter adapter);
        public Object onGetItem(SimpleAdapter adapter, int i);
        public View onGetView(SimpleAdapter adapter, int i, View view, ViewGroup viewGroup);
    }

    private ISimpleAdapterCallback callback;

    public SimpleAdapter(ISimpleAdapterCallback callback){
        this.callback = callback;
    }


    public void setCallback(ISimpleAdapterCallback callback) {
        this.callback = callback;
    }

    @Override
    public int getCount() {
        if (callback!=null) return callback.onGetCount(this);
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if (callback!=null) return callback.onGetItem(this, i);
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (callback!=null) return callback.onGetView(this, i, view, viewGroup);
        return null;
    }
}
