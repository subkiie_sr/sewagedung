package com.subki.sewagedung;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.subki.sewagedung.model.ModelRuang;
import com.subki.sewagedung.utils.Helper;

public class ScreenRuangDetail extends AppCompatActivity {

    public static ModelRuang.Item itemTemp = new ModelRuang.Item();

    public static void startActivy(Context context, ModelRuang.Item item){

        itemTemp = item;

        Intent intent = new Intent(context, ScreenRuangDetail.class);
        intent.putExtra("ruangId", item.ruangId);
        intent.putExtra("ruangName", item.ruangName);
        intent.putExtra("description", item.description);
        intent.putExtra("size", item.size);
        intent.putExtra("price", item.price);
        intent.putExtra("urlImage", item.urlImage);
        context.startActivity(intent);
    }


    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_ruang_detail);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra("ruangName"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ImageView img = (ImageView) findViewById(R.id.imageRuang);
        Picasso.with(this).load(getIntent().getStringExtra("urlImage")).into(img);
        String desc = getIntent().getStringExtra("description");
        String size = getIntent().getStringExtra("size");
        int pr = getIntent().getIntExtra("price", 0);
        String price = Helper.FormatCurrency("indonesia", pr);
        String description = getString(R.string.deskripsi, desc, size, price);
        TextView tv = (TextView) findViewById(R.id.description);
        tv.setText(description);

        findViewById(R.id.booking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                booking();
            }
        });
    }

    private void booking() {
        Toast.makeText(this, itemTemp.ruangName, Toast.LENGTH_SHORT).show();
        ScreenPesanRuang.startActivy(this, itemTemp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
