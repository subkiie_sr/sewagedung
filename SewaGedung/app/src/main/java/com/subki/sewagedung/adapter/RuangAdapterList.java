package com.subki.sewagedung.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.subki.sewagedung.R;
import com.subki.sewagedung.model.ModelRuang;
import com.subki.sewagedung.utils.Helper;

import java.util.List;

/**
 * Created by Erick on 29/09/2016.
 */

public class RuangAdapterList extends RecyclerView.Adapter<RuangAdapterList.MyViewHolder>{

    private List<ModelRuang.Item> mList;
    private Context mContext;
    private OnItemClickListener listener;

    public interface OnItemClickListener{
        void OnItemClick(ModelRuang.Item item, int position);
    }

    public RuangAdapterList(Context mContext, List<ModelRuang.Item> mList, OnItemClickListener listener) {
        this.mList = mList;
        this.mContext = mContext;
        this.listener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nama, size, harga;
        private ImageView icon;

        public MyViewHolder(View view) {
            super(view);
            nama = (TextView) view.findViewById(R.id.nama);
            size = (TextView) view.findViewById(R.id.size);
            harga = (TextView) view.findViewById(R.id.harga);
            icon = (ImageView) view.findViewById(R.id.image);
        }

        public void bind(final ModelRuang.Item item, final int position){
            nama.setText(item.ruangName);
            size.setText("Type "+item.size);
            harga.setText(Helper.FormatCurrency("indonesia", item.price));

            Picasso.with(mContext).load(item.urlImage).error(R.drawable.ic_launcher).resize(400,400).centerCrop().into(icon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnItemClick(item, position);
                }
            });

        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_ruang, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(mList.get(position), position);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

}
