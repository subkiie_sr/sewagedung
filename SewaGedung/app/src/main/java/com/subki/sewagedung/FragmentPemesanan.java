package com.subki.sewagedung;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.subki.sewagedung.adapter.PemesananAdapter;
import com.subki.sewagedung.model.ModelPemesanan;
import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.DividerItemDecoration;
import com.subki.sewagedung.utils.Helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subki on 02/03/2017.
 */

public class FragmentPemesanan extends Fragment implements ModelPemesanan.OnPemesananListener, PemesananAdapter.OnItemClickListener {

    private static final String TAG = "FragmentHome";

    private View root;

    private List<ModelPemesanan.Item> itemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PemesananAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_pemesanan,container,false);


        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        mAdapter = new PemesananAdapter(getContext(), itemList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        recyclerView.setAdapter(mAdapter);

        String userId = Helper.readString(Constan.USER_ID, "");
        ModelPemesanan.getPemesanan(userId, this);

        root.findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderPemesanan();
            }
        });
        return root;
    }

    private void orderPemesanan() {
        startActivity(new Intent(getContext(), ScreenPesanRuang.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        String userId = Helper.readString(Constan.USER_ID, "");
        ModelPemesanan.getPemesanan(userId, this);
    }

    @Override
    public void OnPemesanan(List<ModelPemesanan.Item> itemList, Exception e, Throwable t) {
        Log.d(TAG, "OnPemesanan");
        if(e==null && t==null){
            this.itemList.clear();
            this.itemList.addAll(itemList);
            mAdapter.notifyDataSetChanged();
        }else{
            if(e!=null) Helper.showError(getContext(), "Exception Error : "+e.toString());
            if(t!=null) Helper.showError(getContext(), "Throwable Error : "+t.toString());
        }
    }

    @Override
    public void OnItemClick(ModelPemesanan.Item item, int position) {

    }
}
