package com.subki.sewagedung;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.subki.sewagedung.model.ModelUser;
import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.Helper;

import java.util.List;

/**
 * Created by subki on 02/03/2017.
 */

public class FragmentProfile extends Fragment implements ModelUser.OnGetUserInformastionLitener, View.OnClickListener {

    private static final String TAG = "FragmentProfile";

    private View root;

    private InputMethodManager imm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_profile,container,false);

        imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        String userId = Helper.readString(Constan.USER_ID,"");
        ModelUser.getUserInfo(userId, this);

        root.findViewById(R.id.userId).setOnClickListener(this);
        root.findViewById(R.id.nama).setOnClickListener(this);
        root.findViewById(R.id.username).setOnClickListener(this);
        root.findViewById(R.id.alamat).setOnClickListener(this);
        root.findViewById(R.id.ktp).setOnClickListener(this);
        root.findViewById(R.id.hp).setOnClickListener(this);
        root.findViewById(R.id.email).setOnClickListener(this);
        root.findViewById(R.id.password).setOnClickListener(this);


        root.findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editUserInfo();
            }
        });
        return root;
    }


    private TextView textView;

    @Override
    public void onClick(View v) {
        String header = "";
        textView =(TextView) v;
        switch (v.getId()){
            case R.id.username :
                header = "User Name";
                break;
            case R.id.nama :
                header = "Full Name";
                break;
            case R.id.alamat :
                header = "Alamat";
                break;
            case R.id.ktp :
                header = "No KTP";
                break;
            case R.id.hp :
                header = "No HP";
                break;
            case R.id.email :
                header = "Alamat Email";
                break;
            case R.id.password :
                header = "Password";
                break;
        }

        final View view = ViewGroup.inflate(getContext(),R.layout.pop_up_text,null);
        final EditText isi = (EditText) view.findViewById(R.id.field_message);
        final TextView message= (TextView) view.findViewById(R.id.message);
        message.setText(header);
        if(v.getId() == R.id.password){
            isi.setText("");
        }else {
            isi.setText(textView.getText().toString());
        }
        imm.showSoftInput(isi,0);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        imm.hideSoftInputFromWindow(isi.getWindowToken(),0);
                        textView.setText(isi.getText().toString());
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        imm.hideSoftInputFromWindow(isi.getWindowToken(), 0);
                        dialog.dismiss();
                    }
                });
        builder.show();
        isi.requestFocus();
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void editUserInfo() {
        String userid, fullname, username, ktp, alamat, email, hp, password;
        TextView tv = null;
        tv = (TextView) root.findViewById(R.id.userId);
        userid = tv.getText().toString();
        tv = (TextView) root.findViewById(R.id.nama);
        fullname =  tv.getText().toString();
        tv = (TextView) root.findViewById(R.id.username);
        username = tv.getText().toString();
        tv = (TextView) root.findViewById(R.id.alamat);
        alamat = tv.getText().toString();
        tv = (TextView) root.findViewById(R.id.ktp);
        ktp = tv.getText().toString();
        tv = (TextView) root.findViewById(R.id.hp);
        hp = tv.getText().toString();
        tv = (TextView) root.findViewById(R.id.email);
        email = tv.getText().toString();
        tv = (TextView) root.findViewById(R.id.password);
        password = tv.getText().toString();

        ModelUser.editUser(userid, fullname, username, ktp, alamat, email, hp, password, new ModelUser.OnEditUserListener() {
            @Override
            public void OnEdited(String result, Exception e, Throwable t) {
                Log.d(TAG, "OnEdited");
                if(e==null && t==null){
                    Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
                }else{
                    if(e!=null) Helper.showError(getContext(), "Exception Error : "+e.toString());
                    if(t!=null) Helper.showError(getContext(), "Throwable Error : "+t.toString());
                }
            }
        });
    }

    @Override
    public void OnGetUserInformastion(List<ModelUser.Item> itemList, Exception e, Throwable t) {
        Log.d(TAG, "OnGetUserInformastion");
        if(e==null && t==null){
            for(int i=0; i<itemList.size(); i++){
                ModelUser.Item item = itemList.get(i);
                TextView tv = null;
                tv = (TextView) root.findViewById(R.id.userId);
                tv.setText(item.userId);

                tv = (TextView) root.findViewById(R.id.nama);
                tv.setText(item.fullName);

                tv = (TextView) root.findViewById(R.id.username);
                tv.setText(item.userName);

                tv = (TextView) root.findViewById(R.id.alamat);
                tv.setText(item.alamat);

                tv = (TextView) root.findViewById(R.id.ktp);
                tv.setText(item.ktp);

                tv = (TextView) root.findViewById(R.id.hp);
                tv.setText(item.hp);

                tv = (TextView) root.findViewById(R.id.email);
                tv.setText(item.email);

                tv = (TextView) root.findViewById(R.id.password);
                tv.setText("************");

            }
        }else{
            if(e!=null) Helper.showError(getContext(), "Exception Error : "+e.toString());
            if(t!=null) Helper.showError(getContext(), "Throwable Error : "+t.toString());
        }
    }

}
