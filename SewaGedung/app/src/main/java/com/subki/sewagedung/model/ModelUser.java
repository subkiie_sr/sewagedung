package com.subki.sewagedung.model;

import android.util.Log;

import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by subki on 09/04/2017.
 */

public class ModelUser {

    public static final String TAG = "ModelUser";

    public interface OnGetUserInformastionLitener{
        void OnGetUserInformastion(List<Item> itemList, Exception e, Throwable t);
    }
    public interface OnLoginListener{
        void OnLogin(Item itemList, Exception e, Throwable t);
    }
    public interface OnEditUserListener{
        void OnEdited(String result, Exception e, Throwable t);
    }
    public interface OnSavedListener{
        void OnSaved(JSONObject object, Exception e, Throwable t);
    }
    public static class Item{
        public String userId;
        public String userName;
        public String fullName;
        public String ktp;
        public String alamat;
        public String email;
        public String hp;
        public String password;
    }

    public static void userLogin(String username, String password, final OnLoginListener listener){
        Log.d("Login", username+" "+password);
        String function = "userLogin";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.loginApp(function, username, password);
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                Item item = null;
                try {

                    JSONObject object = new JSONObject(str);
                    Log.d(TAG, object.toString());

                    item = new Item();

                    if(object.has("userId")) item.userId = object.getString("userId");
                    if(object.has("fullName")) item.fullName = object.getString("fullName");
                    if(object.has("userName")) item.userName = object.getString("userName");
                    if(object.has("ktp")) item.ktp = object.getString("ktp");
                    if(object.has("alamat")) item.alamat = object.getString("alamat");
                    if(object.has("email")) item.email = object.getString("email");
                    if(object.has("hp")) item.hp = object.getString("hp");
                    if(object.has("password")) item.password = object.getString("password");

                    listener.OnLogin(item, null, null);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "Error : "+e.toString());
                    listener.OnLogin(item, e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnLogin(null, null, t);
            }
        });
    }

    public static void getUserInfo(String userId, final OnGetUserInformastionLitener listener){
        String function = "userInformation";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.userInfromation(function, userId);
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                List<Item> items = null;
                try {
                    JSONArray arr = new JSONArray(str);
                    items = new ArrayList<>();
                    for(int i=0; i<arr.length(); i++){
                        Item item = new Item();
                        JSONObject object = arr.getJSONObject(i);
                        Log.d(TAG, object.toString());
                        if(object.has("userId")) item.userId = object.getString("userId");
                        if(object.has("fullName")) item.fullName = object.getString("fullName");
                        if(object.has("userName")) item.userName = object.getString("userName");
                        if(object.has("ktp")) item.ktp = object.getString("ktp");
                        if(object.has("alamat")) item.alamat = object.getString("alamat");
                        if(object.has("email")) item.email = object.getString("email");
                        if(object.has("hp")) item.hp = object.getString("hp");
                        if(object.has("password")) item.password = object.getString("password");
                        items.add(item);
                    }
                    listener.OnGetUserInformastion(items, null, null);
                } catch (JSONException e) {
                    listener.OnGetUserInformastion(items, e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnGetUserInformastion(null, null, t);
            }
        });
    }

    public static void editUser(String userId, String fullName, String userName, String ktp, String alamat, String email, String hp, String password, final OnEditUserListener listener){
        String function = "editUser";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.editUserInformaion(function, userId, fullName, userName, alamat, ktp, hp, email, password);
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                try {
                    JSONObject object = new JSONObject(str);
                    Log.d(TAG, object.toString());
                    if(object.has("result")) listener.OnEdited(object.getString("result"), null, null);
                }catch (JSONException e) {
                    listener.OnEdited("gagal", e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnEdited("gagal", null, t);
            }
        });
    }

    public static void saveUser(String fullName, String userName, String ktp, String alamat, String email, String hp, String password, final OnSavedListener listener){
        String function = "saveUser";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.saveUser(function,  fullName, userName, alamat, ktp, hp, email, password);
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                JSONObject object = null;
                try {
                    object = new JSONObject(str);
                    listener.OnSaved(object, null, null);
                } catch (JSONException e) {
                    listener.OnSaved(null, e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnSaved(null, null,t);
            }
        });
    }

}
