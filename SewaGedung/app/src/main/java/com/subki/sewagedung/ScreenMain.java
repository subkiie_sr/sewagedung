package com.subki.sewagedung;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.Helper;

public class ScreenMain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "Screenmain";

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private InputMethodManager imm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_main);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.nvView);
        navigationView.setNavigationItemSelectedListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout,toolbar,R.string.openDrawer,R.string.closeDrawer){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        loadViewDefault();
        loadHeaderNav(navigationView);

    }

    private void loadHeaderNav(NavigationView nv) {
        View header = nv.getHeaderView(0);
        TextView nama = (TextView) header.findViewById(R.id.username);
        TextView email = (TextView) header.findViewById(R.id.email);

        String ngaran = Helper.readString(Constan.USER_NAME,"");
        String userId = Helper.readString(Constan.USER_ID,"");
        String ema = Helper.readString(Constan.USER_EMAIL, "");

        Log.d(TAG, ngaran+" "+ema+" "+userId);

        nama.setText(ngaran);
        email.setText(ema);
//        Picasso.with(this).load(R.drawable.a32).into(img);
        header.findViewById(R.id.ipaddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editIP();
            }
        });
    }

    private void editIP() {
        final View view = ViewGroup.inflate(this,R.layout.pop_up_text,null);
        final EditText isi = (EditText) view.findViewById(R.id.field_message);
        final TextView message= (TextView) view.findViewById(R.id.message);
        message.setText("Masukkan IP Address Local Anda");
        isi.setText(Constan.IPADDRESS);

        imm.showSoftInput(isi,0);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        imm.hideSoftInputFromWindow(isi.getWindowToken(),0);
                        Constan.IPADDRESS = isi.getText().toString();
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        imm.hideSoftInputFromWindow(isi.getWindowToken(), 0);
                        dialog.dismiss();
                    }
                });
        builder.show();
        isi.requestFocus();
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void loadViewDefault() {
        getSupportActionBar().setTitle("Home");
        getSupportActionBar().setLogo(R.drawable.ic_home);
        FragmentHome home = new FragmentHome();
        android.support.v4.app.FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction2.replace(R.id.frame,home);
        fragmentTransaction2.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        if(item.isChecked()) item.setChecked(false);
        else item.setChecked(true);

        drawerLayout.closeDrawers();

        switch (item.getItemId()){
            case R.id.home :
                getSupportActionBar().setTitle("Home");
                getSupportActionBar().setLogo(R.drawable.ic_home);
                FragmentHome home = new FragmentHome();
                android.support.v4.app.FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction2.replace(R.id.frame,home);
                fragmentTransaction2.commit();
                return true;
            case R.id.tipe :
                getSupportActionBar().setTitle("Type (S, M, L, XL)");
                getSupportActionBar().setLogo(R.drawable.ic_tipe);
                FragmentTipe tipe = new FragmentTipe();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame,tipe);
                fragmentTransaction.commit();
                return true;
            case R.id.pemesanan :
                getSupportActionBar().setTitle("Pemesanan");
                getSupportActionBar().setLogo(R.drawable.ic_pesanan);
                FragmentPemesanan pesan = new FragmentPemesanan();
                android.support.v4.app.FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction1.replace(R.id.frame,pesan);
                fragmentTransaction1.commit();
                return true;
            case R.id.profile :
                getSupportActionBar().setTitle("Profile");
                getSupportActionBar().setLogo(R.drawable.ic_profile);
                FragmentProfile pesanan = new FragmentProfile();
                android.support.v4.app.FragmentTransaction fragmentTransaction3 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction3.replace(R.id.frame,pesanan);
                fragmentTransaction3.commit();
                return true;
            case R.id.about :
                getSupportActionBar().setTitle("About");
                getSupportActionBar().setLogo(R.drawable.ic_about);
                FragmentAbout about = new FragmentAbout();
                android.support.v4.app.FragmentTransaction fragmentTransaction4 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction4.replace(R.id.frame,about);
                fragmentTransaction4.commit();
                return true;
            case R.id.logout :
                startActivity(new Intent(ScreenMain.this, ScreenLogin.class));
                finish();
                return true;
            default:
                return true;
        }
    }
}
