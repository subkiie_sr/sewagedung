package com.subki.sewagedung.model;

import android.util.Log;

import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by subki on 09/04/2017.
 */

public class ModelPemesanan {

    public static final String TAG = "ModelPemesanan";

    public interface OnPemesananListener{
        void OnPemesanan(List<Item> itemList, Exception e, Throwable t);
    }
    public interface OnSavePemesananListener{
        void OnSaved(String result, Exception e, Throwable t);
    }
    public static class Item{
        public String pemesananId;
        public String ruangId;
        public String ruangName;
        public String ruangImage;
        public String ruangSize;
        public String userId;
        public String userName;
        public String tanggal;
        public String durasi;
        public String status;
        public String createDate;
    }


    public static void getPemesanan(String userId, final OnPemesananListener listener){
        String function = "pemesanan";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.listPemesanan(function, userId);
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                List<Item> items = null;
                try {
                    JSONArray arr = new JSONArray(str);
                    items = new ArrayList<>();
                    for(int i=0; i<arr.length(); i++){
                        Item item = new Item();
                        JSONObject object = arr.getJSONObject(i);
                        Log.d(TAG, object.toString());
                        if(object.has("pemesananId")) item.ruangId = object.getString("pemesananId");
                        if(object.has("ruangId")) item.ruangId = object.getString("ruangId");
                        if(object.has("ruangName")) item.ruangName = object.getString("ruangName");
                        if(object.has("userId")) item.userId = object.getString("userId");
                        if(object.has("userName")) item.userName = object.getString("userName");
                        if(object.has("ruangImage")) item.ruangImage = Constan.BASE_IMAGE+object.getString("ruangImage");
                        if(object.has("ruangSize")) item.ruangSize = object.getString("ruangSize");
                        if(object.has("tanggal")) item.tanggal = object.getString("tanggal");
                        if(object.has("durasi")) item.durasi = object.getString("durasi");
                        if(object.has("status")) item.status = object.getString("status");
                        items.add(item);
                    }
                    listener.OnPemesanan(items, null, null);
                } catch (JSONException e) {
                    listener.OnPemesanan(items, e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnPemesanan(null, null, t);
            }
        });
    }

    public static void savePemesanan(String ruangId, String userId, String tanggal, String durasi, final OnSavePemesananListener listener){
        String function = "savePemesanan";
        Api api = Helper.retrofit().create(Api.class);
        Call<ResponseBody> r = api.savePemesanan(function, ruangId, userId, tanggal, durasi, "Waiting Payment");
        r.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String str = "";
                try{
                    if(response.isSuccessful()){
                        str = response.body().string();
                        Log.d(TAG, str);
                    }
                }catch (IOException es){
                    es.printStackTrace();
                }

                try {
                    JSONObject object = new JSONObject(str);
                    Log.d(TAG, object.toString());
                    if(object.has("result")) listener.OnSaved(object.getString("result"), null, null);
                }catch (JSONException e) {
                    listener.OnSaved("gagal", e, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnSaved("gagal", null, t);
            }
        });
    }

}
