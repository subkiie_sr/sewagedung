package com.subki.sewagedung.model;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by subki on 09/04/2017.
 */

public interface Api {

    @GET("model.php")
    Call<ResponseBody> loginApp(
            @Query("function") String function,
            @Query("username") String username,
            @Query("password") String password
    );

    @GET("model.php")
    Call<ResponseBody> userInfromation(
            @Query("function") String function,
            @Query("userId") String userId
    );

    @GET("model.php")
    Call<ResponseBody> editUserInformaion(
            @Query("function") String function,
            @Query("userId") String userId,
            @Query("fullName") String fullName,
            @Query("userName") String userName,
            @Query("alamat") String alamat,
            @Query("ktp") String ktp,
            @Query("hp") String hp,
            @Query("email") String email,
            @Query("password") String password
    );

    @GET("model.php")
    Call<ResponseBody> saveUser(
            @Query("function") String function,
            @Query("fullName") String fullName,
            @Query("userName") String userName,
            @Query("alamat") String alamat,
            @Query("ktp") String ktp,
            @Query("hp") String hp,
            @Query("email") String email,
            @Query("password") String password
    );

    @GET("model.php")
    Call<ResponseBody> listAllRuangan(
            @Query("function") String function
    );

    @GET("model.php")
    Call<ResponseBody> ruanganBySize(
            @Query("function") String function,
            @Query("size") String size
    );

    @GET("model.php")
    Call<ResponseBody> ruanganCategory(
            @Query("function") String function
    );

    @GET("model.php")
    Call<ResponseBody> listPemesanan(
            @Query("function") String function,
            @Query("userId") String userId
    );

    @GET("model.php")
    Call<ResponseBody> savePemesanan(
            @Query("function") String function,
            @Query("ruangId") String ruangId,
            @Query("userId") String userId,
            @Query("tanggal") String tanggal,
            @Query("durasi") String durasi,
            @Query("status") String status
    );

}
