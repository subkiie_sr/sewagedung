package com.subki.sewagedung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.subki.sewagedung.model.ModelUser;
import com.subki.sewagedung.utils.Constan;
import com.subki.sewagedung.utils.Helper;

import org.json.JSONException;
import org.json.JSONObject;

public class ScreenRegister extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_register);

        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveuser();
            }
        });
    }

    private void saveuser() {
        String fullname, username, ktp, alamat, email, hp, password;
        EditText ed = null;
        ed = (EditText) findViewById(R.id.nama);
        fullname = ed.getText().toString();
        ed = (EditText) findViewById(R.id.ktp);
        ktp = ed.getText().toString();
        ed = (EditText) findViewById(R.id.alamat);
        alamat = ed.getText().toString();
        ed = (EditText) findViewById(R.id.email);
        email = ed.getText().toString();
        ed = (EditText) findViewById(R.id.telepon);
        hp = ed.getText().toString();
        ed = (EditText) findViewById(R.id.username);
        username = ed.getText().toString();
        ed = (EditText) findViewById(R.id.password);
        password = ed.getText().toString();
        ModelUser.saveUser(fullname, username, ktp, alamat, email, hp, password, new ModelUser.OnSavedListener() {
            @Override
            public void OnSaved(JSONObject object, Exception e, Throwable t) {
//                Log.d(TAG, "OnPemesanan");
                if(e==null && t==null){
                    try {
                        if(object.has("userId"))Helper.writeString(Constan.USER_ID, object.getString("userId"));
                        if(object.has("fullName"))Helper.writeString(Constan.USER_NAME, object.getString("fullName"));
                        if(object.has("email"))Helper.writeString(Constan.USER_EMAIL, object.getString("email"));
                    } catch (JSONException e1) {
                        Helper.showError(ScreenRegister.this, "JSONException Error : "+e1.toString());
                    }
                }else{
                    if(e!=null) Helper.showError(ScreenRegister.this, "Exception Error : "+e.toString());
                    if(t!=null) Helper.showError(ScreenRegister.this, "Throwable Error : "+t.toString());
                }


            }
        });
    }
}
