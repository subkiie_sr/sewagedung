package com.subki.sewagedung;

import android.app.Application;
import android.content.Context;

/**
 * Created by MR on 6/9/2016.
 */
public class AppMain extends Application {
    final String TAG = "AppMain";

    private static AppMain instance;

    public static AppMain getInstance() {
        return instance;
    }

    public static Context getContext(){
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();


        instance = this;

    }
}
